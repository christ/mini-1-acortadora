import random
import string
import urllib.parse
from webapp import WebApp
import shelve


class ShortUrl(WebApp):
    urls = shelve.open("urls")

    #Utilizamos esta funcion para poder cerrar seguro el shelve y que se cierre adecuadamente.
    def __del__(self):
        self.urls.close()

    def parse(self, request):
        parts = request.split()
        method = parts[0]
        resource = urllib.parse.unquote(parts[1])
        body = request.split('\r\n\r\n')[-1]
        print("Method:", method)
        print("Resource:", resource)
        print("Body:", body)
        return method, resource, body

    def process(self, parsedRequest):
        method, resource, body = parsedRequest

        form = """
            <form action = "" method="POST">
                <p>URL Original: <input type="text" name="url_original"/> </p>
                <p><input type="submit" value="SHORTER!"/></p>
            </form>
        """
        http_code_error = "404 Not Found"
        html_body_error = '<html><body><meta charset="UTF-8"/>' \
                          + "<p>El recurso " + resource + " no se encuentra disponible</p>" \
                          + '</body></html>'

        if method == "GET":
            if resource in self.urls.keys():
                http_code = "200 OK"
                html_code = "<html><body>Redirigiendo a: " \
                            + '<a href="' + str(self.urls[resource]) + '">' + str(self.urls[resource]) + '</a><br></body></html>'
            elif resource == "/":
                http_code = "200 OK"
                html_code = "<html><body>Acortadora de URLs" \
                            "<p>Formulario para acortar urls:</p>" + form \
                            + "<p>Urls acortadas: </p>"
                for key, value in self.urls.items():
                    html_code += '<a href="' + str(value) + '">' + str(key) + '</a><br>'
                html_code += "</body></html>"
            else:
                http_code = http_code_error
                html_code = html_body_error

        if method == "POST":
            url = urllib.parse.unquote(body.split('&')[0].split("=")[1])
            if not url.startswith("http://") and not url.startswith("https://"):
                url = "http://" + url
            short = '/' + ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(6))

            if url != "":
                self.urls[short] = url
                http_code = "200 OK"
                html_code = "<html><body>URL original: " \
                            + '<a href="' + str(self.urls[short]) + '">' + str(self.urls[short]) + '</a><br>' \
                            + "<p>URL Shorter: " \
                            + '<a href="' + short + '">' + short + '</a></p></body></html>'
            else:
                http_code = http_code_error
                html_code = html_body_error

        return http_code, html_code


if __name__ == "__main__":
    testWebApp = ShortUrl("localhost", 1234)