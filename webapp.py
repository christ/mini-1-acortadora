import socket


class WebApp:
    """Root of a hierarchy of classes implementing web aplications
    This class does almost nothing. Usually, new classes will inherit from it,
    and by redefining "parse" and "process" methods will implement the logic of
    a web application in particular"""

    def parse(self, request):
        """Parse the received request, extracting the relevant information."""

        return None

    def process(self, parsedRequest):
        """Process the relevant elements of the request.
        Returns the http code for the reply, and an HTML page
         """

        return ("200 OK", "<html><body><h1>It works!</h1></body></html>")

    def __init__(self, hostname, port):
        """Initialize the web application"""

        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((hostname, port))

        mySocket.listen(5)

        try:
            while True:
                print('Waiting for connections')
                (recvSocket, address) = mySocket.accept()
                print('HTTP request received (going to parse and process):')
                request = recvSocket.recv(2048).decode('utf-8')
                print(request)
                parsed_request = self.parse(request)
                (returnCode, htmlAnswer) = self.process(parsed_request)
                print('Answering back...')
                recvSocket.send(bytes("HTTP/1.1 " + returnCode + " \r\n\r\n" +
                                      htmlAnswer + "\r\n", 'utf-8'))
                recvSocket.close()
        except KeyboardInterrupt:
            print("Closing binded socket")
            mySocket.close()


if __name__ == "__main__":
    testWebApp = WebApp("localhost", 1234)
